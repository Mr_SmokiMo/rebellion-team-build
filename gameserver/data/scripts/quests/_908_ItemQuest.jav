package quests;

import l2r.commons.util.Rnd;
import l2r.gameserver.model.Party;
import l2r.gameserver.model.Player;
import l2r.gameserver.model.instances.NpcInstance;
import l2r.gameserver.model.quest.Quest;
import l2r.gameserver.model.quest.QuestState;
import l2r.gameserver.scripts.ScriptFile;

/**
 * @author SmokiMo
 */
public abstract class _908_ItemQuest extends Quest implements ScriptFile
{
	private static final int NPC = 300301;
	private static final int BOSS = 25450;
	private static final int[] MOBS = {22132,22130,22131,22135};
	
	private static final int FEATHER = 2129;
	private static final int FEATHER_COUNT_MIN = 1;
	private static final int FEATHER_COUNT_MAX = 1;
	private static final int FEATHER_NEED = 100;
	private static final int FEATHER_CHANCE = 100;
	private static final int BOSS_ITEM = 2125;

	private static final int WINNER = 7058;
	private static final int WINNER_COUNT_MIN = 1;
	private static final int WINNER_COUNT_MAX = 1;
	
	public _908_ItemQuest()
	{
		super(PARTY_ALL);
		addStartNpc(NPC);
		addKillId(BOSS);
		addKillId(MOBS);
		addQuestItem(BOSS_ITEM, WINNER);
	}
	
	@Override
	public String onEvent(String event, QuestState st, NpcInstance npc)
	{
		String htmltext = event;
		int count = Rnd.get(WINNER_COUNT_MIN, WINNER_COUNT_MAX);
		
		if(event.equalsIgnoreCase("accept.htm"))
		{
			st.setState(STARTED);
			st.playSound("ItemSound.quest_accept");
			st.set("cond","1");
		}
		else if (event.equalsIgnoreCase("finish.htm"))
		{
			if (st.getQuestItemsCount(FEATHER) >= FEATHER_NEED && st.getQuestItemsCount(BOSS_ITEM) >= 1)
			{
				st.takeItems(FEATHER, -1);
				st.takeItems(BOSS_ITEM, -1);
				st.giveItems(WINNER, count);
				st.set("cond","0");
				st.playSound("ItemSound.quest_finish");
				st.setState(COMPLETED);
			}
			else
			{
				htmltext = "mobs.htm";
			}
		}
		return htmltext;
	}
	
	@Override
	public String onTalk(NpcInstance npc, QuestState st)
	{
		String htmltext = "noquest";
		int cond = st.getCond();
		Player player = st.getPlayer();
		
		if (cond == 0)
		{
			return htmltext;
		}
		
		int npcId = npc.getNpcId();
		int idd = st.getState();
		cond = st.getInt("cond");
		
		if (npcId == NPC)
		{
			if (idd == COMPLETED)
			{
				htmltext = "completed.htm";
			}
		}
		if (idd == CREATED)
		{
			htmltext = "hello1.htm";
		}
		else if (cond == 1)
		{
				if (st.getQuestItemsCount(FEATHER) >= FEATHER_NEED && st.getQuestItemsCount(BOSS_ITEM) >= 1)
				{
					htmltext = "mobs2.htm";
				}
				else
				{
					htmltext = "mobs.htm";
				}
		}
		return htmltext;
	}

	/*@Override
	public String onKill(NpcInstance npc, QuestState st)
	{
		int npcId = npc.getNpcId();
		int cond = st.getCond();
		Player player = st.getPlayer();
		Party party = player.getParty();
		
   if (cond != 1)
     return
    		 
   int reward = 0;
   int limit = 1;
   int intchance = 100;
   int count = 1;
   if (npcId = MOBS)
	   reward = FEATHER;
   limit = FEATHER_NEED;
   chance = FEATHER_CHANCE;
   count = Rnd.get(FEATHER_COUNT_MIN, FEATHER_COUNT_MAX);
   if (npcId == BOSS)
	   reward = BOSS_ITEM;
   limit = 1;
   count = 1;
   if (reward > 0)
	   party = player.getParty();
   if (party)
	   for (member in party.getPartyMembers())
		   if (not member.isAlikeDead())
			   st = member.getQuestState("q902_FeatherGathering");
			   if (st and st.getQuestItemsCount(reward) < limit and st.getRandom(100) <= chance)
				   st.giveItems(reward, count);
			   st.playSound("ItemSound.quest_itemget");
			   if (st.getQuestItemsCount(reward) >= limit)
				   st.playSound("ItemSound.quest_middle");
			   else
				   if (st.getQuestItemsCount(reward) < limit and st.getRandom(100) <= chance)
					   st.giveItems(reward, count);
			   st.playSound("ItemSound.quest_itemget");
			   return 
	}*/
		   
			@Override
			public void onLoad()
			{
			}

			@Override
			public void onReload()
			{
			}

			@Override
			public void onShutdown()
			{
			}
}