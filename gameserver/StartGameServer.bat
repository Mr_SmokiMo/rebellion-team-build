@echo off
title Rebellion-Team: Game Server Console
:start
echo Starting Rebellion-Team GameServer.
echo.

REM ������� � �������� ���������� ������
REM ����������� �������� ��� ������� � ��������: 1.5G
REM ����������� �������� ��� ������� ��� �������: 1G
REM -Xms � -Xmx ������ ���� ������ ����� ���� �����.
set JAVA_OPTS=%JAVA_OPTS% -Xms1024m
set JAVA_OPTS=%JAVA_OPTS% -Xmx1024m

REM ��������� �������� ������ � �����������
set JAVA_OPTS=%JAVA_OPTS% -Xnoclassgc
set JAVA_OPTS=%JAVA_OPTS% -XX:+AggressiveOpts
set JAVA_OPTS=%JAVA_OPTS% -XX:TargetSurvivorRatio=90
set JAVA_OPTS=%JAVA_OPTS% -XX:SurvivorRatio=16
set JAVA_OPTS=%JAVA_OPTS% -XX:MaxTenuringThreshold=12
set JAVA_OPTS=%JAVA_OPTS% -XX:+UseParNewGC
set JAVA_OPTS=%JAVA_OPTS% -XX:+UseConcMarkSweepGC
REM set JAVA_OPTS=%JAVA_OPTS% -XX:+CMSIncrementalMode
set JAVA_OPTS=%JAVA_OPTS% -XX:+CMSIncrementalPacing
set JAVA_OPTS=%JAVA_OPTS% -XX:+CMSParallelRemarkEnabled
REM ��� 64-������ ������ ����� -XX:+UseCompressedOops ��������� ������ ������ � ����� ������������������
set JAVA_OPTS=%JAVA_OPTS% -XX:+UseCompressedOops
set JAVA_OPTS=%JAVA_OPTS% -XX:UseSSE=3
set JAVA_OPTS=%JAVA_OPTS% -XX:+UseFastAccessorMethods

java -server -Dfile.encoding=UTF-8 %JAVA_OPTS% -cp config;./libs/* l2r.gameserver.GameServer

REM Debug ...
REM java -Xmx1G -Xnoclassgc -Xdebug -Xnoagent -Djava.compiler=NONE -Xrunjdwp:transport=dt_socket,server=y,suspend=y,address=7456 l2r.gameserver.GameServer

if ERRORLEVEL 2 goto restart
if ERRORLEVEL 1 goto error
goto end
:restart
echo.
echo Server restarted ...
echo.
goto start
:error
echo.
echo Server terminated abnormaly ...
echo.
:end
echo.
echo Server terminated ...
echo.

pause
