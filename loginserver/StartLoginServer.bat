@echo off
title Rebellion-Team: Login Server Console
:start
echo Starting Rebellion-Team LoginServer.
echo.
java -server -Dfile.encoding=UTF-8 -Xms32m -Xmx64m -cp config;./libs/* l2r.loginserver.LoginServer
if ERRORLEVEL 2 goto restart
if ERRORLEVEL 1 goto error
goto end
:restart
echo.
echo Server restarted ...
echo.
goto start
:error
echo.
echo Server terminated abnormaly ...
echo.
:end
echo.
echo Server terminated ...
echo.

pause
