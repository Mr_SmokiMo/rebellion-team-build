configbase(){
echo "Please enter MySQL Server hostname (default localhost): "
read DBHOST
if [ -z "$DBHOST" ]; then
  DBHOST="localhost"
fi
echo "\nPlease enter MySQL Server name (default l2rdb): "
read DBHOST
if [ -z "$DBNAME" ]; then
  DBNAME="l2rdb"
fi
echo -"\nPlease enter MySQL Server user (default root): "
read USER
if [ -z "$USER" ]; then
  USER="root"
fi
echo -"\nPlease enter MySQL Server password (default root): "
read PASS
if [ -z "$PASS" ]; then
  PASS="root"
fi

ask
}


ask(){
clear
echo "Rebellion-team database installer."
echo "Choose full (f) if you want to install login, game and community base, "
echo "your base will be erased."
echo "Choose dump (d) to dump your database."
echo "Choose Quit (q) to quit from installer."
echo "Choose: (f) full, (d) dump or (q) quit? "
read ASKACT
case "$ASKACT" in
	"f"|"F") install;;
	"d"|"D") dump;;
	"q"|"Q") finish;;
	*) ask;;
}

dump(){
mysqldump --ignore-table=${DBNAME}.game_log --ignore-table=${DBNAME}.loginserv_log --ignore-table=${DBNAME}.petitions --add-drop-table -h $DBHOST -u $USER --password=$PASS $DBNAME > l2rdb_full_backup.sql
}

install(){
echo "Installing loginserver."
for login in $(ls ./login/*.sql);do
	echo "Installing Loginserver table : $login"
	mysql -h $DBHOST -u $USER --password=$PASS -D $DBNAME < $login
done
clear
echo "Installling communityboard."
for cb in $(ls ./communityboard_pvp/*.sql);do
	echo "Installing Community Board table : $cb"
	mysql -h $DBHOST -u $USER --password=$PASS -D $DBNAME < $cb
done
clear
echo "Installing gameserver."
mysql -h $DBHOST -u $USER --password=$PASS -D $DBNAME < tools/gs_cleanup.sql
for gs in $(ls ./game/*.sql);do
	echo "Installing Gameserver table : $gs"
	mysql -h $DBHOST -u $USER --password=$PASS -D $DBNAME < $gs
done
}

finish(){exit 0}