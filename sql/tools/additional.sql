# ���� �� ������� ����� ����������
SELECT * FROM `item_template` WHERE (`sellprice` * `buycount` > `buyprice`) and entry in (select item from npc_vendor);
SELECT * FROM `item_template` WHERE (`buycount` = 0) AND (`sellprice` > `buyprice`) and entry in (select item from npc_vendor);

#������ ������ �� ������� (��� "SELECT *" ����� ����� ������ �� "DELETE" )
SELECT * FROM `character_action` where guid not in (select guid from `character`);
SELECT * FROM `character_aura` where guid not in (select guid from `character`);
SELECT * FROM `character_homebind` where guid not in (select guid from `character`);
SELECT * FROM `character_inventory` where guid not in (select guid from `character`);
SELECT * FROM `character_kill` where guid not in (select guid from `character`);
SELECT * FROM `character_pet` where guid not in (select guid from `character`);
SELECT * FROM `character_queststatus` where guid not in (select guid from `character`);
SELECT * FROM `character_reputation` where guid not in (select guid from `character`);
SELECT * FROM `character_social` where guid not in (select guid from `character`);
SELECT * FROM `character_spell` where guid not in (select guid from `character`);
SELECT * FROM `character_spell_cooldown` where guid not in (select guid from `character`);
SELECT * FROM `character_stable` where guid not in (select guid from `character`);
SELECT * FROM `character_ticket` where guid not in (select guid from `character`);
SELECT * FROM `character_tutorial` where guid not in (select guid from `character`);
SELECT * FROM `corpse` where `player` not in (select guid from `character`);
SELECT * FROM `character_inventory` where item not in (SELECT guid FROM `item_instance`);
SELECT * FROM `item_instance` where guid not in (SELECT item FROM `character_inventory`);
SELECT * FROM `character_queststatus` where quest not in (select `entry` from `quest_template`);
SELECT * FROM `character_social` where `friend` not in (select guid from `character`);

# �������� �������� � ��������� ("SELECT *" -> "DELETE" ��� �������)
SELECT * FROM `mangos`.`npc_vendor` WHERE `item` not in (SELECT `entry` FROM `item_template`);

# �������� ��� ������� ����, ������ NPC �� ����� �� ������� (flag �� �������� 4)
# �������:
# ��� �������� ���� (���� ��� ������������� ��������),
# ��� ������� ������ �� npc_vendor (���� �� ��������)
SELECT * FROM `creature_template` WHERE (`entry` in (SELECT `entry` from `npc_vendor`)) AND (`npcflag` & 4 <> 4);

# �������������� ��������
SELECT * FROM `npc_vendor` WHERE `entry` not in (SELECT `entry` from `creature_template`);

# �������������� �������
SELECT * FROM `npc_trainer` WHERE `entry` not in (SELECT `entry` from `creature_template`);

# ������� �������� �� ������ (������ �� ���� ��� � ����, ��� � � ���� ���������)
SELECT * FROM `mangos`.`quest_template` where minlevel>questlevel

# �������
SELECT * FROM `auctionhouse` where `auctioneerguid` not in (select guid from `character`);
SELECT * FROM `auctionhouse` where `itemowner` not in (select guid from `character`);

# ���� � ���������� / ��������� ������� / ��������
SELECT * FROM `mangos`.`creature` where curhealth < 20;
SELECT * FROM `mangos`.`creature_template` where minhealth>maxhealth;
SELECT * FROM `mangos`.`creature_template` where (minhealth< 20 ) or (maxhealth<20);
SELECT * FROM `mangos`.`creature_template` where minlevel>maxlevel;
SELECT * FROM `mangos`.`creature_template` where (minlevel=0) or (maxlevel=0);

# �������������� ����
SELECT * FROM `creature` c where id not in (select entry from creature_template);